package javarush.crud.service;

import javarush.crud.model.User;

import java.util.List;

public interface UserService {
    public void addUser(User user);

    public void updateUser(User user);

    public void removeUser(int id);

    public User getUserById(int id);

    public List<User> pageUsers(Integer offset, Integer maxResults, String name);

    public  Long count(String name);

}

package javarush.crud.dao;

import javarush.crud.model.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {
    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(user);
        logger.info("User successfully saved. User details: " + user);
    }

    @Override
    public void updateUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(user);
        logger.info("User successfully update. User details: " + user);
    }

    @Override
    public void removeUser(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        User user = (User) session.load(User.class, new Integer(id));

        if(user != null){
            session.delete(user);
        }
        logger.info("User successfully removed. User details: " + user);
    }

    @Override
    public User getUserById(int id) {
        Session session =this.sessionFactory.getCurrentSession();
        User user = (User) session.load(User.class, new Integer(id));
        logger.info("User successfully loaded. User details: " + user);

        return user;
    }



    @Override
    @SuppressWarnings("unchecked")
    public List<User> pageUsers(Integer offset, Integer maxResults, String userName) {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session
                .createCriteria(User.class);
        if(userName != null)
            if(!userName.equals("")){
            criteria.add(Restrictions.eq("name", userName));
        }
        criteria.setFirstResult(offset!=null?offset:0)
                .setMaxResults(maxResults!=null?maxResults:10);
        List<User> userList = criteria.list();

        for(User user : userList){
            logger.info("User page: " + user);
        }

        return userList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Long count(String userName){
        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(User.class);
        if(userName != null)
            if(!userName.equals("")){
                criteria.add(Restrictions.eq("name", userName));
            }
        return (Long) criteria.setProjection(Projections.rowCount())
                .uniqueResult();
    }
}
